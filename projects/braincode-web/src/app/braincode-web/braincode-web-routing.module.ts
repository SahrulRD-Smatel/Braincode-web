import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { BraincodeWebComponent } from './braincode-web.component';
import { CareersComponent } from './careers/careers.component';
import { AndroidProgrammerComponent } from './careers/sub-careers/android-programmer.component';
import { ClientsComponent } from './clients/clients.component';
import { NewsComponent } from './news/news.component';


const routes: Routes = [
  {
    path: '',
    component: BraincodeWebComponent,
  },
  {
    path: 'about',
    children: [
      { path: '', component: AboutComponent },
      // { path: '', component:  },
    ]
  },
  {
    path: 'careers',
    children: [
      { path: '', component: CareersComponent },
      { path: 'android-programmer', component:  AndroidProgrammerComponent},
    ]
  }, 
  {
    path: 'clients',
    children: [
      { path: '', component: ClientsComponent },
      // { path: '', component:  },
    ]
  },
  {
    path: 'news',
    children: [
      { path: '', component: NewsComponent },
      // { path: '', component:  },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BraincodeWebRoutingModule {}
