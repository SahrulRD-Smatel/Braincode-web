import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BraincodeWebRoutingModule } from './braincode-web-routing.module';
import { BraincodeWebComponent } from './braincode-web.component';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [BraincodeWebComponent],
  imports: [CommonModule, BraincodeWebRoutingModule,MatButtonModule]
})
export class BraincodeWebModule {}
